alter table chapter drop foreign key fk_chapter_comic_id;
drop index ix_chapter_comic_id on chapter;

alter table comic_category drop foreign key fk_comic_category_comic;
drop index ix_comic_category_comic on comic_category;

alter table comic_category drop foreign key fk_comic_category_category;
drop index ix_comic_category_category on comic_category;

alter table comic_catalog drop foreign key fk_comic_catalog_comic;
drop index ix_comic_catalog_comic on comic_catalog;

alter table comic_catalog drop foreign key fk_comic_catalog_catalog;
drop index ix_comic_catalog_catalog on comic_catalog;

drop table if exists catalog;

drop table if exists category;

drop table if exists chapter;

drop table if exists comic;

drop table if exists comic_category;

drop table if exists comic_catalog;

