namespace java vn.hcmus.fit.truyenfull.lib_data

typedef i64 long
typedef i32 int


struct tComic{
	1: long id;
	2: string name;
	3: string urlname;
	4: string author;
	5: string source;
	6: string status;
	//7: double rating;
	//8: int vote_count;
	//9: string description;
	7: list<tChapter> chapterList;
	8: list<tCategory> categoryList;
	//12: list<tCategory> catalogList;
}

struct tChapter{
	1: long id;
	2: long index;
	3: string name;
	4: string content;
	5: tComic comic;
}

struct tCategory{
	1: long id;
	2: string name;
	3: string urlname;
	4: list<tComic> comicList;
}

service TruyenFullDataService{
	// Comic
	// CRUD comic
	string getAll(1:int page,2:int limit);
	string getAComic(1:long id);
	string updateComic(1:long id,2:tComic comic);
	string addComic(1:tComic comic);
	string deleteComic(1:long id);
	
	// Get comics based on some criteria		
	string getTopRatedComics();
	string getLatestComic(1:int page,2:int limit)
	string getFinishedComics(1:int page,2:int limit);
	string getHotComics(1:int page,2:int limit);
	string getComicsByCategory(1:long catId,2:int page,3:int limit);
	string getComicsByAuthor(1:string autName,2:int page,3:int limit);
	
	
	// Get some properties of a comic
	string getReviewsOfComic(1:long id);
	string getChaptersOfComic(1:long id,2:int page,3:int limit);
	string getCategoriesOfComic(1:long id);
	
	// Searching
	string searchComic(1:string query,2:int page,3:int limit);
}