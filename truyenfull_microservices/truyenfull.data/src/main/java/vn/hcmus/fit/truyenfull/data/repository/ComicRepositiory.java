package vn.hcmus.fit.truyenfull.data.repository;


import io.ebean.EbeanServer;
import io.ebean.PagedList;
import io.ebean.Query;
import io.ebean.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.hcmus.fit.truyenfull.data.model.Comic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;


@Repository
public class ComicRepositiory {

    @Autowired
    private EbeanServer server;

    public <T> Stream<T> findAll(Class<T> clazz,int page,int limit) {
        return server.find(clazz).setFirstRow(page).setMaxRows(limit).findList().stream();
    }

    public <T> PagedList<T> findAllPaging(Class<T> clazz, int page, int limit) {
        return server.find(clazz).setFirstRow(page).setMaxRows(limit).findPagedList();
    }

    public <T> T findById(Class<T> clazz, long id) {
        return server.find(clazz, id);
    }

    public <T> T findOneBy(Class<T> clazz, String column, Object val) {
        return server.find(clazz).where().eq(column, val).findOne();
    }

    public <T> PagedList<T> findListBy(Class<T> clazz, String column,Object val, int page, int limit){
        return server.find(clazz).where().eq(column,val).setFirstRow(page).setMaxRows(limit).findPagedList();
    }

    public <T> PagedList<T> findTop(Class<T> clazz,String column,int size){
        return server.find(clazz).orderBy().desc(column).setMaxRows(size).findPagedList();
    }


    public PagedList<Comic> findByCriteria(long idCatalog,int page,int limit){
        String sql = " select *"
        +" from comic a"
        +" join comic_catalog b on a.id = b.comic_id and b.catalog_id = ?";

        List<Comic> comics =  server.findNative(Comic.class,sql)
                .setParameter(1,idCatalog).findList();

        List<Long> ids = new ArrayList<>();

        comics.stream().forEach(c-> ids.add(c.getId()));

        return server.find(Comic.class).where().in("id",ids).setFirstRow(page).setMaxRows(limit).findPagedList();
    }

    public PagedList<Comic> findByCatId(long catId,int page,int limit){
        String sql = " select *"
                +" from comic a"
                +" join comic_category b on a.id = b.comic_id and b.category_id = ?";

        List<Comic> comics =  server.findNative(Comic.class,sql)
                .setParameter(1,catId).findList();

        List<Long> ids = new ArrayList<>();

        comics.stream().forEach(c-> ids.add(c.getId()));

        return server.find(Comic.class).where().in("id",ids).setFirstRow(page).setMaxRows(limit).findPagedList();
    }

    public <T> Query<T> query(Class<T> clazz) {
        return server.find(clazz);
    }

    public <T> PagedList<T> search(Class<T> clazz, String query,int page,int limit){
        return server.find(clazz).where().like("name","%"+query+"%").setFirstRow(page).setMaxRows(limit).findPagedList();
    }

    @Transactional
    public <T> void save(T bean) {
        server.save(bean);
    }

    @Transactional
    public <T> void update(T bean) {
        server.update(bean);
    }

    @Transactional
    public <T> void saveAll(Collection<T> beans) {
        server.saveAll(beans);
    }

    @Transactional
    public <T> void delete(T bean) {
        server.delete(bean);
    }

    @Transactional
    public <T> void deleteById(Class<T> clazz, int id) {
        server.delete(clazz, id);
    }

    @Transactional
    public <T> void deleteAll(Collection<T> beans) {
        server.deleteAll(beans);
    }

    @Transactional
    public <T> void deleteAllById(Class<T> clazz, List<Integer> ids) {
        server.deleteAll(clazz, ids);
    }

}
