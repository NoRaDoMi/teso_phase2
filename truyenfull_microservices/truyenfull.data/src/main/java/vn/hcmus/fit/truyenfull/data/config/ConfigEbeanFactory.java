package vn.hcmus.fit.truyenfull.data.config;

import io.ebean.EbeanServer;
import io.ebean.EbeanServerFactory;
import io.ebean.config.ServerConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class ConfigEbeanFactory implements FactoryBean<EbeanServer> {

    private static final Logger LOGGER = LogManager.getLogger(ConfigEbeanFactory.class);

    @Override
    public EbeanServer getObject() throws Exception {

	try {
	    ServerConfig cfg = new ServerConfig();
	    Properties properties = new Properties();

		properties.put("ebean.db.ddl.generate", "true");
		properties.put("ebean.db.ddl.run", "false");
		properties.put("datasource.db.username", "root");
		properties.put("datasource.db.password", "0939414836");
		properties.put("datasource.db.databaseUrl", "jdbc:mysql://localhost:3306/db_truyen?useUnicode=yes&characterEncoding=UTF-8&character_set_server=utf8mb4");
		properties.put("datasource.db.databaseDriver", "com.mysql.cj.jdbc.Driver");
		properties.put("ebean.search.packages", "vn.hcmus.fit.truyenfull.data.model");

	    cfg.loadFromProperties(properties);
	    return EbeanServerFactory.create(cfg);
	} catch (Exception e) {
	    LOGGER.error(e.getStackTrace());
//	    SlackUtils.sendMessage(ExceptionUtils.getMessage(e), ExceptionUtils.getStackTrace(e));
	    return null;
	}
    }

    @Override
    public Class<?> getObjectType() {
	return EbeanServer.class;
    }

    @Override
    public boolean isSingleton() {
	return true;
    }
}
