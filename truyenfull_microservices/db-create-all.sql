-- init script create procs
-- Inital script to create stored procedures etc for mysql platform
DROP PROCEDURE IF EXISTS usp_ebean_drop_foreign_keys;

delimiter $$
--
-- PROCEDURE: usp_ebean_drop_foreign_keys TABLE, COLUMN
-- deletes all constraints and foreign keys referring to TABLE.COLUMN
--
CREATE PROCEDURE usp_ebean_drop_foreign_keys(IN p_table_name VARCHAR(255), IN p_column_name VARCHAR(255))
BEGIN
  DECLARE done INT DEFAULT FALSE;
  DECLARE c_fk_name CHAR(255);
  DECLARE curs CURSOR FOR SELECT CONSTRAINT_NAME from information_schema.KEY_COLUMN_USAGE
    WHERE TABLE_SCHEMA = DATABASE() and TABLE_NAME = p_table_name and COLUMN_NAME = p_column_name
      AND REFERENCED_TABLE_NAME IS NOT NULL;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN curs;

  read_loop: LOOP
    FETCH curs INTO c_fk_name;
    IF done THEN
      LEAVE read_loop;
    END IF;
    SET @sql = CONCAT('ALTER TABLE ', p_table_name, ' DROP FOREIGN KEY ', c_fk_name);
    PREPARE stmt FROM @sql;
    EXECUTE stmt;
  END LOOP;

  CLOSE curs;
END
$$

DROP PROCEDURE IF EXISTS usp_ebean_drop_column;

delimiter $$
--
-- PROCEDURE: usp_ebean_drop_column TABLE, COLUMN
-- deletes the column and ensures that all indices and constraints are dropped first
--
CREATE PROCEDURE usp_ebean_drop_column(IN p_table_name VARCHAR(255), IN p_column_name VARCHAR(255))
BEGIN
  CALL usp_ebean_drop_foreign_keys(p_table_name, p_column_name);
  SET @sql = CONCAT('ALTER TABLE ', p_table_name, ' DROP COLUMN ', p_column_name);
  PREPARE stmt FROM @sql;
  EXECUTE stmt;
END
$$
create table catalog (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  urlname                       varchar(255),
  constraint uq_catalog_name unique (name),
  constraint uq_catalog_urlname unique (urlname),
  constraint pk_catalog primary key (id)
);

create table category (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  urlname                       varchar(255),
  description                   TEXT,
  constraint uq_category_name unique (name),
  constraint uq_category_urlname unique (urlname),
  constraint pk_category primary key (id)
);

create table chapter (
  id                            bigint auto_increment not null,
  index_chapter                 bigint,
  name                          varchar(255),
  content                       TEXT,
  comic_id                      bigint,
  constraint pk_chapter primary key (id)
);

create table comic (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  urlname                       varchar(255),
  author                        varchar(255),
  source                        varchar(255),
  status                        varchar(255),
  rating                        double not null,
  vote_count                    integer not null,
  description                   TEXT,
  constraint uq_comic_name unique (name),
  constraint uq_comic_urlname unique (urlname),
  constraint pk_comic primary key (id)
);

create table comic_category (
  comic_id                      bigint not null,
  category_id                   bigint not null,
  constraint pk_comic_category primary key (comic_id,category_id)
);

create table comic_catalog (
  comic_id                      bigint not null,
  catalog_id                    bigint not null,
  constraint pk_comic_catalog primary key (comic_id,catalog_id)
);

create index ix_chapter_comic_id on chapter (comic_id);
alter table chapter add constraint fk_chapter_comic_id foreign key (comic_id) references comic (id) on delete restrict on update restrict;

create index ix_comic_category_comic on comic_category (comic_id);
alter table comic_category add constraint fk_comic_category_comic foreign key (comic_id) references comic (id) on delete restrict on update restrict;

create index ix_comic_category_category on comic_category (category_id);
alter table comic_category add constraint fk_comic_category_category foreign key (category_id) references category (id) on delete restrict on update restrict;

create index ix_comic_catalog_comic on comic_catalog (comic_id);
alter table comic_catalog add constraint fk_comic_catalog_comic foreign key (comic_id) references comic (id) on delete restrict on update restrict;

create index ix_comic_catalog_catalog on comic_catalog (catalog_id);
alter table comic_catalog add constraint fk_comic_catalog_catalog foreign key (catalog_id) references catalog (id) on delete restrict on update restrict;

